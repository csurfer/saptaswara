This is the repository to my google chrome extension Saptaswara. This extension makes it easy to listen to music 
from sites which play the .mp3/.avi files directly in the browser's player rather than their own custom player.

Here is a link to the extension on chrome store : 
https://chrome.google.com/webstore/detail/saptaswara/bpllbnbickaanafjpfijnpgmadkjoffl
